#include <iostream>
#include "Stack/fixedStack.hpp"
#include "Queue/fixedQueue.hpp"

#ifndef _BINARY_SEARCH_TREE_H
#define _BINARY_SEARCH_TREE_H

template <class T>
class BinarySearchTree
{
public:
	BinarySearchTree();// "�� ���������" ������� ������ ������
	BinarySearchTree(const BinarySearchTree<T>& scr) = delete;
	BinarySearchTree(BinarySearchTree<T>&& scr) noexcept;
	BinarySearchTree <T>& operator= (const BinarySearchTree <T>& src) = delete;
	BinarySearchTree <T>& operator= (BinarySearchTree <T>&& src) noexcept;
	virtual ~BinarySearchTree();

	// 1.1 ������� ������ �� ����� � �������� ������ ������
	bool iterativeSearch(const T& key) const;

	// 2 ������� ������ �������� � ������: true, ���� ������� ��������; 
	// false, ���� ������� ��� ���
	bool insert(const T& key);

	// 3.1 �������� �������� �� ������, �� ���������� ������� ���������
	// true, ���� ������� ������; false, ���� �������� �� ����
	bool deleteKey(const T& key);

	// 4.1 ������ ���������� ����������� ������ � �������� ����� out,
	// ������������ ������, ����� �������� ��������� ������
	void print(std::ostream& out) const;

	// 5.1 ����������� ���������� ����� ������
	int getCount() const;

	// 6.1 ����������� ������ ������
	int getHeight() const;

	// 7 ��������� ����� ������ (�����������) 
	void iterativeInorderWalk() const;

	// 8.1 ��������� ����� ������ (�����������) 
	void inorderWalk() const;

	// 9 ����� ��������� ������ �� ������� (� ������). 
	void walkByLevels() const;

	// 10 �������� �� ��� ������ ��������
	bool isSimilar(const BinarySearchTree<T>& other) const;

	// 11 ���� ���������� ����� � ���� �������� ������
	bool isIdenticalKey(const BinarySearchTree<T>& other) const;

private:
	template <class T>
	struct Node {
		T key_; // �������� �����, ������������ � ����
		Node<T>* left_; // ��������� �� ����� ���������
		Node<T>* right_; // ��������� �� ������ ���������
		Node<T>* p_; // ��������� �� �������� !!! �� ������������
		// ����������� ����
		Node(T key, Node* left = nullptr, Node* right = nullptr, Node* p = nullptr) :
			key_(key), left_(left), right_(right), p_(p)
		{ }
		~Node()
		{
			delete left_;
			delete right_;
		}
	};

	Node<T>* root_; // ��������� �� �������� ����

	// 1.2 ������� ������ ������ ���� �� ����� � �������� ������ ������
	Node<T>* iterativeSearchNode(const T& key) const;

	// 4.2 ����������� ������� ��� ������ ����������� ������ � �������� �����
	void printNode(std::ostream& out, Node<T>* root) const;

	// 5.2 ����������� ������� ����������� ���������� ����� ������
	int getCount(const Node<T>* node) const;

	// 6.2 ����������� ������� ����������� ������ ������
	int getHeight(const Node<T>* node) const;

	// 8.2 ����������� ������� ��� ���������� ������ ����� ������.
	void inorderWalk(Node<T>* node) const;

}; // ����� ������� ������ TreeBinarySearchTree

template<class T>
BinarySearchTree<T>::BinarySearchTree() : root_(nullptr) {}

template<class T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T>&& src) noexcept 
{
	if (this != &src)
	{
		this->root_ = src.root_;
		src.root_ = nullptr;
	}
}

template<class T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(BinarySearchTree<T>&& src) noexcept
{
	if (this == &src)
	{
		return *this;
	}
	this->root_ = src.root_;
	src.root_ = nullptr;
	return *this;
}

template<class T>
BinarySearchTree<T>::~BinarySearchTree()
{
	delete root_;
}

template<class T>
typename BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::iterativeSearchNode(const T& key) const
{
	Node<T> *it = root_;
	while (it != nullptr && it->key_ != key)
	{
		
		if (key >= it->key_)
		{
			it = it->right_;
		}
		else if (key < it->key_)
		{
			it = it->left_;
		}
	}
	return it;
}

template<class T>
bool BinarySearchTree<T>::iterativeSearch(const T& key) const
{
	if (iterativeSearchNode(key) == nullptr)
	{
		return false;
	}

	return true;
}

template <class T>
bool BinarySearchTree<T>::insert(const T& key)
{
	Node<T>* it = root_;
	Node<T>* p = nullptr;
	if (root_ == nullptr)
	{
		root_ = new Node<T>(key);
		return true;
	}
	while (it != nullptr)
	{
		p = it;
		if (key > it->key_) it = it->right_;
		else if (key < it->key_) it = it->left_;
		else if (key == it->key_) return false;
	}
	if (key > p->key_) p->right_ = new Node<T>(key, nullptr, nullptr, p);
	else if (key < p->key_) p->left_ = new Node<T>(key, nullptr, nullptr, p);
	return true;
}

template<class T>
void BinarySearchTree<T>::printNode(std::ostream& out, Node<T>* root) const
{
	if (root == nullptr) return;

	out << "(";
	out << root->key_;
	printNode(out, root->left_);
	printNode(out, root->right_);
	out << ")";
}

template<class T>
void BinarySearchTree<T>::print(std::ostream& out) const
{
	printNode(out, root_);
	out << '\n';
}

template<class T>
int BinarySearchTree<T>::getCount(const Node<T>* node) const
{
	if (node == nullptr) return 1;
	return getCount(node->left_) + getCount(node->right_);
}

template<class T>
int BinarySearchTree<T>::getCount() const
{
	return getCount(root_) - 1;
}

template<class T>
int BinarySearchTree<T>::getHeight(const Node<T>* node) const
{
	if (node == nullptr) return 0;

	return std::max(getHeight(node->left_), getHeight(node->right_)) + 1;
}

template<class T>
int BinarySearchTree<T>::getHeight() const
{
	return getHeight(root_);
}

template<class T>
void BinarySearchTree<T>::inorderWalk(Node<T>* node) const
{
	if (node == nullptr) return;
	inorderWalk(node->left_);
	std::cout << node->key_ << ' ';
	inorderWalk(node->right_);
}

template<class T>
void BinarySearchTree<T>::inorderWalk() const
{
	inorderWalk(root_);
	std::cout << '\n';
}

template<class T>
bool BinarySearchTree<T>::deleteKey(const T& key)
{
	if (getCount() == 1 )
	{
		if (root_->key_ == key) 
		{
			root_ = nullptr;
			return true;
		}
		else
		{
			return false;
		}
	}
	Node<T>* del = iterativeSearchNode(key);
	if (del == nullptr)
	{
		return false;
	}
	del->p_->left_ = del->left_;
	del->p_->right_ = del->right_;
	if (del->left_ != nullptr)
	{
		del->left_->p_ = del->p_;
	}
	if (del->right_ != nullptr)
	{
		del->right_->p_ = del->p_;
	}
	return true;
}

template<class T>
void BinarySearchTree<T>::iterativeInorderWalk() const
{
	int size = getCount();
	FixedStack<Node<T>*> stack(size);
	Node<T>* it = root_;

	while (!stack.isEmpty() || it != nullptr)
	{
		if (it != nullptr)
		{
			stack.push(it);
			it = it->left_;
		}
		else
		{
			it = stack.pop();
			std::cout << it->key_ << ' ';

			it = it->right_;
		}
	}
	std::cout << '\n';
}

template<class T>
void BinarySearchTree<T>::walkByLevels() const
{
	int size = getCount();
	FixedQueue<Node<T>*> queue(size);

	queue.enQueue(root_);
	while (queue.isEmpty() == false)
	{
		Node<T>* node = queue.deQueue();
		std::cout << node->key_ << ' ';

		if (node->left_ != nullptr)
		{
			queue.enQueue(node->left_);
		}
		if (node->right_ != nullptr)
		{
			queue.enQueue(node->right_);
		}
	}
	std::cout << '\n';
}

template<class T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T>& other) const
{
	if (this == &other)
	{
		return true;
	}

	int size = getCount(), size2 = other.getCount();
	FixedStack<Node<T>*> stack(size), stack2(size2);
	T* alph = new T[size], * alph2 = new T[size2];
	int i = 0;

	Node<T>* it = root_;

	while (!stack.isEmpty() || it != nullptr)
	{
		if (it != nullptr)
		{
			stack.push(it);
			it = it->left_;
		}
		else
		{
			it = stack.pop();
			alph[i] = it->key_;
			++i;

			it = it->right_;
		}
	}

	it = other.root_;
	i = 0;
	while (!stack2.isEmpty() || it != nullptr)
	{
		if (it != nullptr)
		{
			stack2.push(it);
			it = it->left_;
		}
		else
		{
			it = stack2.pop();
			alph2[i] = it->key_;
			++i;

			it = it->right_;
		}
	}

	bool ans = true;
	for (int i = 0, j = 0; i < size && j < size2; ++i, ++j)
	{
		if (alph[i] != alph2[j])
		{
			ans = false;
		}
	}

	return ans;

}

template<class T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T>& other) const
{
	int size = getCount(), size2 = other.getCount();
	FixedStack<Node<T>*> stack(size), stack2(size2);
	T* alph = new T[size], * alph2 = new T[size2];
	int i = 0;

	Node<T>* it = root_;

	while (!stack.isEmpty() || it != nullptr)
	{
		if (it != nullptr)
		{
			stack.push(it);
			it = it->left_;
		}
		else
		{
			it = stack.pop();
			alph[i] = it->key_;
			++i;

			it = it->right_;
		}
	}

	it = other.root_;
	i = 0;
	while (!stack2.isEmpty() || it != nullptr)
	{
		if (it != nullptr)
		{
			stack2.push(it);
			it = it->left_;
		}
		else
		{
			it = stack2.pop();
			alph2[i] = it->key_;
			++i;

			it = it->right_;
		}
	}


	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size2; ++j)
		{
			if (alph[i] == alph2[j])
			{
				return true;
			}
		}
	}

	return false;

}

#endif // !_BINARY_SEARCH_TREE_H