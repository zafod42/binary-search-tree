#include <iostream>
#include "BinarySearchTree.h"
#include <assert.h>
#include <string>

void insertTest()
{
	BinarySearchTree<int> bst;
	assert(bst.insert(5) == true);
	assert(bst.insert(5) == false);
	assert(bst.insert(4) == true);
	assert(bst.insert(1) == true);
	std::cout << "Insert test: passed\n";
}

void itSearchTest()
{
	BinarySearchTree<int> bst;
	assert(bst.iterativeSearch(0) == false);
	bst.insert(0);
	assert(bst.iterativeSearch(0) == true);
	std::cout << "Iterative search test: passed\n";
}

void deleteTest()
{
	BinarySearchTree<int> bst;
	bst.insert(4);
	assert(bst.deleteKey(4) == true);
	assert(bst.deleteKey(4) == false);
	assert(bst.deleteKey(10) == false);
	bst.insert(1);
	bst.insert(0);
	bst.insert(2);
	assert(bst.deleteKey(0));
	std::cout << "Delete key test: passed\n";
}

void testPrint()
{
	std::cout << "Print: ";
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	bst.print(std::cout);
}


void testCount()
{
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	assert(bst.getCount() == 3);
	bst.insert(6);
	assert(bst.getCount() != 5);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	assert(bst.getCount() == 7);
	std::cout << "Get count test: passed\n";
}

void testHeight()
{
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	assert(bst.getHeight() == 3);
	bst.insert(6);
	assert(bst.getHeight() == 3);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	assert(bst.getHeight() == 3);
	bst.insert(10);
	assert(bst.getHeight() == 4);
	std::cout << "Get height test: passed\n";
}

void testItInWalk()
{
	std::cout << "Iterative In-order: ";
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	bst.iterativeInorderWalk();
}

void testInWalk()
{
	std::cout << "Recursive In-order: ";
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	bst.inorderWalk();
}

void testWalkByLevels()
{
	std::cout << "Walk-by-levels: ";
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	bst.walkByLevels();
}

void testIsSim()
{
	BinarySearchTree<int> bst;
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);
	bst.insert(5);

	BinarySearchTree<int> bst2;
	bst2.insert(5);
	bst2.insert(7);
	bst2.insert(8);
	bst2.insert(6);
	bst2.insert(3);
	bst2.insert(4);
	bst2.insert(2);

	assert(bst.isSimilar(bst) == true);

	assert(bst.isSimilar(bst2) == true);
	assert(bst2.isSimilar(bst) == true);
	bst2.deleteKey(8);
	assert(bst.isSimilar(bst2) == false);
	assert(bst2.isSimilar(bst) == false);

	std::cout << "Similarity test: passed\n";
}

void testIsIdenticalKey()
{
	BinarySearchTree<int> bst;
	bst.insert(5);
	bst.insert(7);
	bst.insert(8);
	bst.insert(6);
	bst.insert(3);
	bst.insert(4);
	bst.insert(2);

	BinarySearchTree<int> bst2;
	bst2.insert(10);
	bst2.insert(7);
	bst2.insert(0);
	bst2.insert(-12);
	bst2.insert(1);
	bst2.insert(-2);
	bst2.insert(9);

	assert(bst.isIdenticalKey(bst) == true);

	assert(bst.isIdenticalKey(bst2) == true);
	assert(bst2.isIdenticalKey(bst) == true);
	bst2.deleteKey(7);
	assert(bst.isIdenticalKey(bst2) == false);
	assert(bst2.isIdenticalKey(bst) == false);

	std::cout << "Identical key test: passed\n";
}

int main()
{
	insertTest();
	itSearchTest();
	deleteTest();
	testPrint();
	testCount();
	testHeight();
	testItInWalk();
	testIsSim();
	testIsIdenticalKey();
	return 0;
}


