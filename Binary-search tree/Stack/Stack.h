#ifndef __STACK_H
#define __STACK_H

template <class T>
class Stack
{
public:
    virtual ~Stack() = default;
    virtual void push(const T& e) = 0;   // ���������� �������� � ����
    virtual T pop() = 0;                // �������� �������� ��������
    virtual bool isEmpty() = 0;
};

#endif // __STACK_H
