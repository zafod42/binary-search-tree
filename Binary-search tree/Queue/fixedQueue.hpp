#include "Queue.h"
#include "QueueExceptions.h"

#ifndef __FIXEDQUEUE_H
#define __FIXEDQUEUE_H

template<class T>
class FixedQueue : public Queue<T>
{
private:
    T* arr;
    size_t last_pos;
    size_t first_pos;
    const size_t max_size;
    size_t free_places;
public:
    FixedQueue(int mSize);
    FixedQueue(const FixedQueue& src);
    ~FixedQueue();
    void enQueue(const T& e) override;
    T deQueue() override;
    bool isEmpty() override;
    bool isFull();
};

template<class T>
FixedQueue<T>::FixedQueue(int mSize) : max_size(mSize), free_places(mSize), last_pos(0), first_pos(0)
{
    arr = new T[max_size];
}

template<class T> 
FixedQueue<T>::FixedQueue(const FixedQueue& src) : max_size(src.max_size), free_places(src.free_places), 
                                                   last_pos(src.last_pos), first_pos(src.first_pos)
{
    arr = new T[max_size];
    for (int i = 0; i < max_size; ++i)
    {
        arr[i] = src.arr[i];
    }
}

template<class T>
FixedQueue<T>::~FixedQueue()
{
    if (!isEmpty())
    {
        delete[] arr;
    }
}


template<class T>
bool FixedQueue<T>::isFull()
{
    if (free_places == 0)
    {
        return true;
    }
    return false;
}

template<class T>
bool FixedQueue<T>::isEmpty()
{
    if (free_places == max_size)
    {
        return true;
    }
    return false;
}

template<class T>
void FixedQueue<T>::enQueue(const T& element)
{
    try
    {
        if (isFull())
        {
            throw QueueOverflow("QueueOverflow: enQueue to full Queue\n");
        }
        else
        {
            arr[last_pos] = element;
            ++last_pos;
            --free_places;
            if (last_pos == max_size) 
            {
                last_pos = 0;
            }
        }
    }
    catch (QueueOverflow exc)
    {
        std::cerr << exc.what();
    }
}

template<class T>
T FixedQueue<T>::deQueue()
{
    try
    {
        if (isEmpty())
        {
            throw QueueUnderflow("QueueUnderflow: deQueue from empty Queue\n");
        }
        else
        {
            int ret = first_pos;
            ++first_pos;
            ++free_places;
            if (first_pos == max_size)
            {
                first_pos = 0;
            }
            return arr[ret];
        }
    }
    catch (QueueUnderflow exc)
    {
        std::cerr << exc.what();
    }
    return NULL;
}


#endif // !__FIXEDQUEUE_H